<?php

namespace App\Http\Controllers;

use App\Book;
use App\Http\Requests\PostBookRequest;
use App\Http\Resources\BookResource;
use Illuminate\Http\Request;

class BooksController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'auth.admin'])->only(['store']);
    }

    public function index(Request $request)
    {
        // @TODO implement
        $book = Book::select('*');

        if ($request->has('title'))
            $book->where('title', 'LIKE', '%'.$request->query('title').'%');

        if ($request->has('authors'))
            $book->whereHas('authors', function (\Illuminate\Database\Eloquent\Builder $query) use ($request) {
                $query->whereIn('author_id', explode(',', $request->query('authors')));
            });

        if ($request->has('sortColumn'))
            if ($request->query('sortColumn') === 'avg_review')
                $book->orderByRaw('(SELECT AVG(review) AS aggregate FROM book_reviews WHERE book_reviews.book_id = books.id) '.$request->query('sortDirection', 'ASC'));
            elseif ($request->query('sortColumn') === 'count_review')
                $book->orderByRaw('(SELECT COUNT(review) AS aggregate FROM book_reviews WHERE book_reviews.book_id = books.id) '.$request->query('sortDirection', 'ASC'));
            else
                $book->orderBy($request->query('sortColumn', 'id'), $request->query('sortDirection', 'ASC'));

        return BookResource::collection($book->paginate(15));
    }

    public function store(PostBookRequest $request)
    {
        // @TODO implement
        $validated = $request->validated();

        $book = new Book();
        $book->isbn           = $request->input('isbn');
        $book->title          = $request->input('title');
        $book->description    = $request->input('description');
        $book->published_year = $request->input('published_year');
        $book->save();
        $book->authors()->attach($request->input('authors'));

        return new BookResource($book);
    }
}
