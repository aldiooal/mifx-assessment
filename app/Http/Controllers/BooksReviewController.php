<?php

namespace App\Http\Controllers;

use App\BookReview;
use App\Http\Requests\PostBookReviewRequest;
use App\Http\Resources\BookReviewResource;
use Illuminate\Http\Request;

class BooksReviewController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
        $this->middleware(['auth.admin'])->only(['delete']);
    }

    public function store(int $bookId, PostBookReviewRequest $request)
    {
        // @TODO implement
        $book      = \App\Book::findOrFail($bookId);
        $validated = $request->validated();

        $bookReview = new BookReview(['review' => $request->input('review'), 'comment' => $request->input('comment')]);
        $bookReview->user()->associate($request->user());
        $book->reviews()->save($bookReview);

        return new BookReviewResource($bookReview);
    }

    public function destroy(int $bookId, int $reviewId, Request $request)
    {
        // @TODO implement
        $book       = \App\Book::findOrFail($bookId);
        $bookReview = BookReview::findOrFail($reviewId);
        $bookReview->delete();

        return response()->json(null, 204);
    }
}
